$(document).onload(function () {

    $(function () {
        $("#size-control").slider({
            value: 1,
            min: 1,
            max: 100,
            step: 10,
            slide: function (event, ui) {
                $('.dolphin').css('height', ui.value * 5);
            }
        });
    });

    $('#radio-background1').change(function () {
        console.log("msg");
        $('#background').attr('src', '../images/background1.jpg');

    })

    $('#radio-background2').change(function () {
        console.log("msg");
        $('#background').attr('src', '../images/background2.jpg');

    })

    $('#radio-background3').change(function () {
        console.log("msg");
        $('#background').attr('src', '../images/background3.jpg');

    })

    $('#radio-background4').change(function () {
        console.log("msg");
        $('#background').attr('src', '../images/background4.jpg');

    })

    $('#radio-background5').change(function () {
        console.log("msg");
        $('#background').attr('src', '../images/background5.jpg');

    })

    $('#radio-background6').change(function () {
        console.log("msg");
        $('#background').attr('src', '../images/background6.gif');

    })

    $('#check-dolphin1').change(function () {
        console.log("msg");

        if (this.checked) {
            console.log("visible");
            $('#dolphin1').toggle();
        } else {
            console.log("hide");
            $('#dolphin1').hide();
        }

    })

    $('#check-dolphin2').change(function () {
        console.log("msg");
        if (this.checked) {
            console.log("visible");
            $('#dolphin2').toggle();
        } else {
            console.log("hide");
            $('#dolphin2').hide();
        }

    })

    $('#check-dolphin3').change(function () {
        console.log("msg");
        if (this.checked) {
            console.log("visible");
            $('#dolphin3').toggle();
        } else {
            console.log("hide");
            $('#dolphin3').hide();
        }

    })

    $('#check-dolphin4').change(function () {
        console.log("msg");
        if (this.checked) {
            console.log("visible");
            $('#dolphin4').toggle();
        } else {
            console.log("hide");
            $('#dolphin4').hide();
        }

    })

    $('#check-dolphin5').change(function () {
        console.log("msg");
        if (this.checked) {
            console.log("visible");
            $('#dolphin5').toggle();
        } else {
            console.log("hide");
            $('#dolphin5').hide();
        }
    })

    $('#check-dolphin6').change(function () {
        console.log("msg");
        if (this.checked) {
            console.log("visible");
            $('#dolphin6').toggle();
        } else {
            console.log("hide");
            $('#dolphin6').hide();
        }

    })

    $('#check-dolphin7').change(function () {
        console.log("msg");
        if (this.checked) {
            console.log("visible");
            $('#dolphin7').toggle();
        } else {
            console.log("hide");
            $('#dolphin7').hide();
        }

    })

    $('#check-dolphin8').change(function () {
        console.log("msg");
        if (this.checked) {
            console.log("visible");
            $('#dolphin8').toggle();
        } else {
            console.log("hide");
            $('#dolphin8').hide();
        }

    })


})